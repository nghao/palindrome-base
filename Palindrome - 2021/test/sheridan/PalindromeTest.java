package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isPalindrome = Palindrome.isPalindrome("anna");
		assertTrue("Failed to validate",isPalindrome);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean isPalindrome = Palindrome.isPalindrome("hao");
		assertFalse("Failed to validate",isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("race car");
		assertTrue("Failed to validate",isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("race a car");
		assertFalse("Failed to validate",isPalindrome);
	}	
	
}
